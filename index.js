const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');
const spanGreetings = document.querySelector('.span-greetings')

// Alternatives for document.querySelector()
/*
	document.getElementById('txt-firstName');
	document.getElementByClassName('txt-inputs');
	document.getElementByTagName('input');
*/

/*
txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
})
*/

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event.target);
	console.log(event.target.value);
})

// Additional Example & Mini Activity
const greetings = () => {
	let firstName = txtFirstName.value;
	let greeting = 'Hi, welcome! ';
	spanGreetings.innerHTML = greeting + firstName;
	// Hi, welcome firstName

}

txtFirstName.addEventListener('keyup', greetings);

// Activity
const fullName = () => {
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;
	spanFullName.innerHTML = firstName + " " + lastName
}

txtFirstName.addEventListener('keyup', fullName)
txtLastName.addEventListener('keyup', fullName)

